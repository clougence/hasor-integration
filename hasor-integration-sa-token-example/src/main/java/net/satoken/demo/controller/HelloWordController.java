package net.satoken.demo.controller;
import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import net.hasor.core.Inject;
import net.hasor.web.Controller;
import net.hasor.web.Invoker;
import net.hasor.web.WebController;
import net.hasor.web.annotation.Get;
import net.hasor.web.annotation.MappingTo;
import net.hasor.web.annotation.Post;
import net.hasor.web.objects.JsonRenderEngine;
import net.hasor.web.render.RenderType;
import net.satoken.demo.service.MyService;
import net.satoken.demo.utils.AjaxJson;

/**
 * 1. WebController 一些工具方法（可选）
 * 2. Controller 当 Controller 被加载时调用，每个请求都会调用一次。（可选）
 * 3.
 * @author kong
 */
@MappingTo("/test/hello")
@RenderType(value = "json", engineType = JsonRenderEngine.class)
public class HelloWordController extends WebController implements Controller {
    @Inject
    private MyService myService;

    @Override
    public void initController(Invoker renderData) {
        super.initController(renderData);
    }

    @Get            // 接受 Get  请求
    @Post           // 接受 Post 请求
    @SaCheckLogin                   // 注解式鉴权：当前会话必须登录才能通过
    @SaCheckRole("super-admin")     // 注解式鉴权：当前会话必须具有指定角色标识才能通过
    @SaCheckPermission("user-add")  // 注解式鉴权：当前会话必须具有指定权限才能通过
    public AjaxJson atCheck(Invoker invoker) {
        System.out.println("======================= 进入方法，测试注解鉴权接口 ========================= ");
        System.out.println("只有通过注解鉴权，才能进入此方法");
        //		StpUtil.checkActivityTimeout();
        //		StpUtil.updateLastActivityToNow();
        return AjaxJson.getSuccess();
    }
}
