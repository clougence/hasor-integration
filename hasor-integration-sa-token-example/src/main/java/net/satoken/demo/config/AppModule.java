/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.satoken.demo.config;
import cn.dev33.satoken.config.SaTokenConfig;
import net.hasor.core.DimModule;
import net.hasor.integration.satoken.SaTokenApiBinder;
import net.hasor.spring.SpringModule;
import net.hasor.web.WebApiBinder;
import net.hasor.web.WebModule;
import net.hasor.web.annotation.MappingTo;
import org.springframework.stereotype.Component;

/**
 * @author 赵永春 (zyc@hasor.net)
 * @version : 2021-01-02
 */
@DimModule
@Component
public class AppModule implements WebModule, SpringModule {
    @Override
    public void loadModule(WebApiBinder apiBinder) throws Throwable {
        apiBinder.setEncodingCharacter("UTF-8", "UTF-8");
        //
        SaTokenApiBinder tokenApiBinder = apiBinder.tryCast(SaTokenApiBinder.class);
        /** 启用注解 */
        tokenApiBinder.enableAnnotation();
        /** 启用 Web 拦截器 */
        tokenApiBinder.enableWebInterceptor();

        /** 设置配置 */
        tokenApiBinder.setConfig(buildSaTokenConfig());


        // 扫描所有标了 MappingTo 注解的 controller 并注册它们。
        apiBinder.loadMappingTo(apiBinder.findClass(MappingTo.class));

        //        tokenApiBinder.setStpInterface(new CustomStpInterface());
        //
        //tokenApiBinder.setConfig();
        //        // .check dataSource
        //        Objects.requireNonNull(this.metadataDs, "metadataDs is null");
        //        Objects.requireNonNull(this.dataDs1, "dataDs1 is null");
        //        Objects.requireNonNull(this.dataDs2, "dataDs2 is null");
        //        //
        //        // .isolation meta-tables using InformationStorage
        //        apiBinder.bindType(InformationStorage.class).toInstance(() -> {
        //            return this.metadataDs;
        //        });
        //        //
        //        // .add two data sources in to Dataway
        //        apiBinder.installModule(new JdbcModule(Level.Full, "ds1", this.dataDs1));
        //        apiBinder.installModule(new JdbcModule(Level.Full, "ds2", this.dataDs2));
        //        //
        //        // udf/udfSource/import 指令 的类型创建委托给 spring
        //        QueryApiBinder queryBinder = apiBinder.tryCast(QueryApiBinder.class);
        //        queryBinder.bindFinder(Finder.TYPE_SUPPLIER.apply(springTypeSupplier(apiBinder)));
    }

    protected SaTokenConfig buildSaTokenConfig() {
        SaTokenConfig config = new SaTokenConfig();
        config.setTokenName("satoken");        // token名称 (同时也是cookie名称)
        config.setTimeout(30 * 24 * 60 * 60);    // token有效期，单位s 默认30天
        config.setActivityTimeout(-1);        // token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒
        config.setAllowConcurrentLogin(true);    // 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
        config.setIsShare(true);    // 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
        config.setTokenStyle("uuid");        // token风格
        return config;
    }
}
