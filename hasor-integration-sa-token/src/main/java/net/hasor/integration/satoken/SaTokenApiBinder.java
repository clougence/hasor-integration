package net.hasor.integration.satoken;
import cn.dev33.satoken.config.SaTokenConfig;
import net.hasor.core.ApiBinder;

// 给到开发使用的配置接口。
public interface SaTokenApiBinder extends ApiBinder {
    /** 启用 @SaCheckLogin、@SaCheckPermission、@SaCheckRole 三个注解 */
    public SaTokenApiBinder enableAnnotation();

    /** 启用 Web 拦截器，相当于注册了 Filter。*/
    public default SaTokenApiBinder enableWebInterceptor() {
        return enableWebInterceptor(0);//默认 0
    }

    /** 启用 Web 拦截器，相当于注册了 Filter。 参数是用来决定拦截器在 过滤器链中的顺序*/
    public SaTokenApiBinder enableWebInterceptor(int index);

    /** 设置配置 */
    public SaTokenApiBinder setConfig(SaTokenConfig saTokenConfig);
}
