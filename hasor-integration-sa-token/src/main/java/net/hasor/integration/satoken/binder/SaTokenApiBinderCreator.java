package net.hasor.integration.satoken.binder;
import net.hasor.core.ApiBinder;
import net.hasor.core.binder.ApiBinderCreator;
import net.hasor.integration.satoken.SaTokenApiBinder;

public class SaTokenApiBinderCreator implements ApiBinderCreator<SaTokenApiBinder> {
    @Override
    public SaTokenApiBinder createBinder(ApiBinder apiBinder) {
        Boolean enableSaToken = apiBinder.getEnvironment().getSettings().getBoolean("sa-token.enable", false);
        if (enableSaToken != null && enableSaToken) {
            return new SaTokenApiBinderImpl(apiBinder);
        } else {
            return null;
        }
    }
}
