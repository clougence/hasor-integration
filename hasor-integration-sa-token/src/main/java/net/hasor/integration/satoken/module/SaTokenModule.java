package net.hasor.integration.satoken.module;
import net.hasor.core.AppContext;
import net.hasor.core.Environment;
import net.hasor.integration.satoken.SaTokenApiBinder;
import net.hasor.integration.satoken.discoverer.MappingToDiscoverer;
import net.hasor.web.WebApiBinder;
import net.hasor.web.WebModule;
import net.hasor.web.spi.MappingDiscoverer;

public class SaTokenModule implements WebModule {
    @Override
    public void loadModule(WebApiBinder apiBinder) throws Throwable {
        Environment env = apiBinder.getEnvironment();
        SaTokenApiBinder saTokenApiBinder = apiBinder.tryCast(SaTokenApiBinder.class);
        if (saTokenApiBinder == null) {
            throw new IgnoreModuleException(); // saTokenApiBinder 为空表示 SaTokenApiBinderCreator 阶段没有启用 sa-token
        }
        //
        // 声明 Bean
        //
        // 发现所有 controller
        apiBinder.bindSpiListener(MappingDiscoverer.class, new MappingToDiscoverer());
        //
    }

    @Override
    public void onStart(AppContext appContext) throws Throwable {
    }

    @Override
    public void onStop(AppContext appContext) throws Throwable {
    }
}
