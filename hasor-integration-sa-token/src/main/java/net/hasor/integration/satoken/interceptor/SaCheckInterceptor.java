package net.hasor.integration.satoken.interceptor;
import net.hasor.core.MethodInterceptor;
import net.hasor.core.MethodInvocation;

// 注解拦截器
public class SaCheckInterceptor extends AbstractSaCheck implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try {
            System.out.println("BEFORE : " + invocation.getMethod().toString());
            return invocation.proceed();
        } finally {
            System.out.println("AFTER : " + invocation.getMethod().toString());
        }
    }
}
