package net.hasor.integration.satoken.interceptor;
import cn.dev33.satoken.servlet.SaTokenServlet;
import net.hasor.core.AppContext;
import net.hasor.core.spi.AppContextAware;

// 基类
public abstract class AbstractSaCheck implements AppContextAware {
    private SaTokenServlet saTokenServlet;

    @Override
    public void setAppContext(AppContext appContext) {
        appContext.getInstance(SaTokenServlet.class);
    }
}
