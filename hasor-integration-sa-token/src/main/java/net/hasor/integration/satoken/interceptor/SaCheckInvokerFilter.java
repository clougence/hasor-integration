package net.hasor.integration.satoken.interceptor;
import net.hasor.web.Invoker;
import net.hasor.web.InvokerChain;
import net.hasor.web.InvokerFilter;

// Web 拦截器，效果等同于 Filter
public class SaCheckInvokerFilter extends AbstractSaCheck implements InvokerFilter {
    @Override
    public Object doInvoke(Invoker invoker, InvokerChain chain) throws Throwable {
        return chain.doNext(invoker);
    }
}
