package net.hasor.integration.satoken.discoverer;
import net.hasor.web.Mapping;
import net.hasor.web.spi.MappingDiscoverer;

// 发现所有 Hasor 上的控制器
public class MappingToDiscoverer implements MappingDiscoverer {
    @Override
    public void discover(Mapping mapping) {
        System.out.println("!!!discover : " + mapping.getMappingTo());
    }
}
