package net.hasor.integration.satoken.interceptor;
import net.hasor.core.MethodInterceptor;
import net.hasor.core.MethodInvocation;

// SaCheckPermission 注解拦截器
public class SaCheckPermissionInterceptor extends AbstractSaCheck implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        return invocation.proceed();
    }
}
