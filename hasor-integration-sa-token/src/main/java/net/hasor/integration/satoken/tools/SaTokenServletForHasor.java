package net.hasor.integration.satoken.tools;
import cn.dev33.satoken.servlet.SaTokenServlet;
import net.hasor.web.invoker.HttpParameters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * sa-token 对cookie的相关操作 接口实现类
 *
 * @author kong
 *
 */
public class SaTokenServletForHasor implements SaTokenServlet {
    /** 获取当前请求的Request对象 */
    @Override
    public HttpServletRequest getRequest() {
        return HttpParameters.localInvoker().getHttpRequest();
    }

    /** 获取当前请求的Response对象 */
    @Override
    public HttpServletResponse getResponse() {
        return HttpParameters.localInvoker().getHttpResponse();
    }
}
