package net.hasor.integration.satoken.interceptor;
import net.hasor.core.MethodInterceptor;
import net.hasor.core.MethodInvocation;

// SaCheckRole 注解拦截器
public class SaCheckRoleInterceptor extends AbstractSaCheck implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        return invocation.proceed();
    }
}
